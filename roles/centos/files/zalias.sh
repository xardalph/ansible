# ajout d'alias pour plus de facilité avec les commande, fait par evan

alias tree='tree -C'
alias ll='ls -lah --color=auto'

#coloration du texte en vert (utilisateur@machine dossier)

PS1='\[\e[0;32m\]\u@\h \w\$ \[\e[0m\]'
